# GitHub User Working Hour Analysis

[A Representative User-centric Dataset of 10 Million GitHub Developers](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/T6ZRJT)

Chunking the original `data.json`:
```
mkdir chunks && cd chunks
split --numeric-suffixes -n l/$(nproc) ../data.json data.json.
```
