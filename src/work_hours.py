import matplotlib
import matplotlib.pyplot as plt
# %matplotlib inline
import numpy as np
import os
import json
from typing import Dict
from tqdm import tqdm
import sys
import datetime
import seaborn as sns
# import ray
import multiprocessing as mp


SCALE = 10000
INPUT_FILE = '../data_{}.json'.format(SCALE)


def get_company_name(user) -> str:
    if user['company'] is None:
        return None
    company = user['company'].strip('@').lower()
    return company

# y,m,d
def get_create_date_from_repo(repo) -> (int, int, int):
    d = map(int, repo['created_at'].split(' ')[0].split('-'))
    d = list(d)
    return (d[0], d[1], d[2])

# y,m,d
def get_commit_date_from_commit(commit) -> (int, int, int):
    d = map(int, commit['commit_at'].split(' ')[0].split('-'))
    d = list(d)
    return (d[0], d[1], d[2])

# day of week
def get_commit_day_from_commit(commit) -> int:
    y, m, d = get_commit_date_from_commit(commit)
    h, mm, s = get_commit_time_from_commit(commit)
    date = datetime.datetime(y, m, d, h, mm, s, 0)
    # 0-6, 0 Monday, 6 Sunday
    day = date.weekday()
    return (day + 1) % 7

# h,m,s
def get_commit_time_from_commit(commit) -> (int, int, int):
    s = commit['commit_at']
    s = s.split(' ')[1]
    if '.' in s:
        d = map(int, s.split('.')[0].split(':'))
    elif '+' in s:
        d = map(int, s.split('+')[0].split(':'))
    elif '-' in s:
        d = map(int, s.split('-')[0].split(':'))
    elif s[-1] == u'Z':
        # 2000-01-01 00:00:00Z
        d = map(int, s.strip('Z').split(':'))
    else:
        assert False, 'cannot parse, ' + s

    d = list(d)
    return (d[0], d[1], d[2])
example = {'commit_at': '2000-01-01 00:00:00Z'}
get_commit_time_from_commit(example)


repos = {}
repo_list = []
num_users = 0

with tqdm(total=SCALE, file=sys.stdout) as pbar:
    with open(INPUT_FILE, 'r') as f:
        for line in f:
            pbar.update(1)
            line = line.strip()
            r = json.loads(line)
            user_id = r['id']

            # filter out suspicious user accounts
            if r['is_suspicious']: continue
#             company = get_company_name(r)
#             if company is not None:
#                 print(company)

            num_users += 1
            if r['repo_list'] is None: continue
            for repo in r['repo_list']:
                repo_id = repo['id']
                repos[repo_id] = repo
                repo_list.append(repo)

print('analyzing {} users'.format(num_users))
print('num repos:', len(repos))



class Company(object):
    def __init__(self, name):
        self.name = name
        self.heatmap = np.zeros((7, 24))
        self.num_users = 0 # employees
        self.num_repos = 0
        self.num_commits = 0
        self.languages = {} # favorite

    def __repr__(self):
        text = 'name: {}, num_users: {}, num_repos: {}, num_commits: {}, heatmap: {}, languages: {}'.format(self.name, self.num_users, self.num_repos, self.num_commits, self.heatmap, self.languages)
        return text

    def update(self, user):
        self.num_users += 1
        if user['repo_list'] is not None:
            self.num_repos += len(user['repo_list'])
            for repo in user['repo_list']:
                lang = repo['language']
                if lang is None: continue
                self.languages.setdefault(lang, 0)
                self.languages[lang] += 1
        if user['commit_list'] is not None:
            self.num_commits += len(user['commit_list'])
            for c in user['commit_list']:
                self.update_heatmap(c, r)

    def update_heatmap(self, commit, user):
        h, _m, _s = get_commit_time_from_commit(commit)
        day = get_commit_day_from_commit(commit)
        self.heatmap[day][h] += 1


class Companies(object):
    def __init__(self):
        self.companies = {}

    def update(self, user):
        # update companies statistics
        name = get_company_name(user)
        if name is not None:
            self.companies.setdefault(name, Company(name))
            self.companies[name].update(user)

    def __getitem__(self, k):
        return self.companies[k]


def update_lang_heatmap(heatmap: Dict[str, np.ndarray], commit):
    h, m, s = get_commit_time_from_commit(commit)
    repo_id = commit['repo_id']
    if repos.get(repo_id) is None: return
    lang = repos[repo_id]['language']
    heatmap.setdefault(lang, np.zeros((7, 24)))
    day = get_commit_day_from_commit(commit)
    heatmap[lang][day][h] += 1



# "commit_at": "2018-05-24 20:42:37.000+08:00"
def work(args):
    input_file = args[0]
    scale = args[1]
    lang_heatmap = {}
    num_commits = 0

    companies = Companies()
    with tqdm(total=scale, file=sys.stdout) as pbar:
        with open(input_file, 'r') as f:
            for line in f:
                pbar.update(1)
                line = line.strip()
                r = json.loads(line)
                user_id = r['id']
                # filter out suspicious user accounts
                if r['is_suspicious']: continue

                # update companies statistics
                companies.update(r)

                if r['commit_list'] is None: continue
                for c in r['commit_list']:
                    num_commits += 1
                    update_lang_heatmap(lang_heatmap, c)

    print('number of commits:', num_commits)
    print(lang_heatmap['C'])
    print(list(companies.companies.keys()))
    #print(companies['google'])
    return companies

# pool = mp.Pool(processes = 1)
# companies = pool.map(work, [(INPUT_FILE, SCALE)])
companies = work((INPUT_FILE, SCALE))
print(companies['xmu'])
